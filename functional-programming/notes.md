# Functional Programming

## Paradigms

### Procedural
- give example
    - hippo eats watermelon
    ```javascript
    // hippo is a POJsO
    function feedHippos(hippos) {
        hippos.forEach(hippo => {
            // brutal
            hippo.stomach.push("watermelon");
        });
    }
    ```
    - explanation:
        * The hippos are passed by reference, so we're modifying each hippo object.
        * We have to know that the hippo has a stomach. We're basically shoving the watermelon down the hippo's throat.
        * We have changed the hippo's stomach, show this
- pros
    * we see this in real life all the time, so it lends itself to a fairly simple mental model (think recipes)
    * it's the bones of tons of programming languages (C, C++, Java, Javascript, Go, etc...)
- data/code relationship
    - we know what the hippo looks like inside
    - we know the things we can do to the hippo
    - data and code are separate
    - mutation happens
### OOP
- give example
    - hippo eats watermelon
    ```javascript
    class Hippo {
        constructor() {
            this.stomach = [];
        }

        const eat = (food) => {
            // less brutal
            this.stomach.push(food);
        }
    }

    // Elsewhere at the zoo...

    function feedHippos(hippos) {
        hippos.forEach(hippo => {
            hippo.eat("watermelon");
        });
    }
    ```
     - explanation:
        * The hippos are passed by reference, so we're still modifying each hippo object.
        * We do NOT have to know that the hippo has a stomach. We've encapsulated this.
        * The hippos changes its own stomach
- pros
    - good at mimicking the real world
    - ubiquitous, provides common mental model, experience base, and a common language
- data/code relationship
    - internals hidden/encapsulated
    - data & code are combined
    - mutation happens
### Declarative
- give example
```html
<HippoPen>
    <Hippo onWatermelonProximityAlert={eatWatermelon}>
</HippoPen>
```
- pros
- data/logic relationship
### Functional
- give example
- pros
- data/logic relationship
    - mutation happens
    - data & code are separate